export abstract class BaseModel {
    constructor(data?: {}) {
        if (data) {
            this.mapToProperties(data);
        }
    }

    protected abstract mapToProperties(data: any): void;

    protected convertIsoStringToDate(isoDateString: string): Date | null {
        if (isoDateString === null || isoDateString === undefined) {
            return null;
        }

        if (!isNaN(Date.parse(isoDateString))) {
            return new Date(isoDateString);
        }

        return null;
    }
}
