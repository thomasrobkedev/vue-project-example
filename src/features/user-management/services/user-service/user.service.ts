import {Vue} from 'vue-property-decorator';
import axios from 'axios';

export default class UserService extends Vue {
    public apiClient: any;

    constructor() {
        super();

        this.apiClient = axios.create({
            baseURL: 'http://localhost:3000',
            withCredentials: false,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        });
    }

    public getUsers(start: number = 0, limit: number = 100) {
        return this.apiClient.get('/users', {
            params: {
                _start: start,
                _limit: limit,
            },
        });
    }
}
