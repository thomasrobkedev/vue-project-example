import {Component, Vue} from 'vue-property-decorator';
import UserService from '@/features/user-management/services/user-service/user.service';
import User from '@/features/user-management/models/user/user.model';
import {mapGetters, mapMutations, mapActions} from 'vuex';

@Component({
    computed: mapGetters(['foo', 'users']),
    methods: mapActions(['updateUsers', 'updateUsersAsync']),
})
export default class Users extends Vue {
    private errorMessage: string = '';
    private userService: UserService;

    constructor() {
        super();

        // @todo: inject the service
        this.userService = new UserService();
    }

    private created() {
        this.userService.getUsers(0, 10)
            .then(this.onGetSuccess)
            .catch(this.handleError);
    }

    private onGetSuccess(response: any) {
        const users = response.data.map((item: any) => new User(item));

        this.$store.commit('updateUsers', users);
        // this.$store.dispatch('updateUsersAsync', users);
    }

    private handleError(errorMessage: string) {
        this.errorMessage = errorMessage;
    }
}
