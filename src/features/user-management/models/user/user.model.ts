import {UserInterface} from './user.interface';
import {BaseModel} from '@/models/base-model';

export default class User extends BaseModel implements UserInterface {
    public id!: number;
    public firstName!: string;
    public lastName!: string;
    public gender!: string;
    public birthdate!: Date | null;
    public email!: string;
    public company!: string;
    public department!: string;
    public jobTitle!: string;
    public avatar!: string;

    public mapToProperties(data?: any) {
        this.id = data.id || null;
        this.firstName = data.first_name || null;
        this.lastName = data.last_name || null;
        this.gender = data.gender || null;
        this.birthdate = this.convertIsoStringToDate(data.birthdate || null);
        this.email = data.email || null;
        this.company = data.company || null;
        this.department = data.department || null;
        this.jobTitle = data.job_title || null;
        this.avatar = 'http://img.robke.dev/'
            + (data.gender === 'Female' ? 'female/' : 'male/')
            + (data.id % 69) + '.jpg';
    }

    public getName(): string {
        return this.firstName + ' ' + this.lastName;
    }
}
