import User from '@/features/user-management/models/user/user.model';

describe('UserModel', () => {
    it('should create an instance', () => {
        const user = new User();
        expect(user).toBeTruthy();
    });

    it('should create an instance from empty object', () => {
        const user = new User({});
        expect(user.id).toEqual(null);
    });

    it('should create an instance from partial object', () => {
        const data = {
            id: 3,
            first_name: 'John',
            birthdate: '1999-10-25',
        };
        const user = new User(data);

        expect(user.id).toEqual(3);
        expect(user.firstName).toEqual('John');
        expect(user.birthdate instanceof Date).toBeTruthy();
    });
});
