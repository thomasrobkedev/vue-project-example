export interface UserInterface {
    id: number;
    firstName: string;
    lastName: string;
    gender: string;
    birthdate: Date | null;
    email: string;
    company: string;
    department: string;
    jobTitle: string;
    avatar: string;

    getName(): string;
}
