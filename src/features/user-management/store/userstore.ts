import User from '@/features/user-management/models/user/user.model';

const state = {
    foo: 1,
    users: [],
};

const getters = {
    foo(currentState): number {
        return currentState.foo;
    },

    users(currentState): User[] {
        return currentState.users;
    },
};

const mutations = {
    updateUsers(currentState, users: User[]) {
        currentState.users = users;
    },
};

const actions = {
    updateUsers({commit}, users: User[]) {
        commit('updateUsers', users);
    },
    updateUsersAsync({commit}, users: User[]) {
        setTimeout(() => {
            commit('updateUsers', users);
        }, 2000);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};
