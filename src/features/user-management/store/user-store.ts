import User from '@/features/user-management/models/user/user.model';
import UserService from '@/features/user-management/services/user-service/user.service';

const userService = new UserService();

/**
 * Funktioniert mit der Klasse noch nicht richtig.
 * Wird darum derzeit nicht genutzt, weil ohne Prio.
 * Trotzdem bitte nicht weg schmeißen.
 */
export default class UserStore {
    public getStore() {
        return {
            state: this.getState(),
            getters: this.getGetters(),
            mutations: this.getMutations(),
            actions: this.getActions(),
        };
    }

    public getMutations() {
        return {
            updateUsers(currentState, users: User[]) {
                currentState.users = users;
            },
        };
    }

    public getActions() {
        return {
            updateUsers({commit}, users: User[]) {
                commit('updateUsers', users);
            },
            updateUsersAsync({commit}, users: User[]) {
                setTimeout(() => {
                    commit('updateUsers', users);
                }, 2000);
            },
        };
    }

    public getState() {
        return {
            foo(currentState): number {
                return currentState.foo;
            },
            users(currentState): any[] {
                return currentState.users;
            },
        };
    }

    public getGetters() {
        return {
            foo(currentState): number {
                return currentState.foo;
            },

            users(currentState): any[] {
                return currentState.users;
            },
        };
    }
}

//
// export default {
//     state,
//     getters,
//     mutations,
//     actions,
// };
