import Vue from 'vue';
import Router from 'vue-router';
import Users from '@/features/user-management/components/users/users';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/',
            redirect: {name: 'users'},
        },
    ],
});
