import Vue from 'vue';
import Vuex from 'vuex';
import userstore from '@/features/user-management/store/userstore';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        userstore,
    },
});
