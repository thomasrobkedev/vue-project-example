# vue-project-example


## Vorraussetzungen
* node, npm und vue-cli sollten global installiert und aktuell sein.
* `node -v`
  * z.B. 10.16.0
  
* `npm -v` 
  * z.B. 6.10.0
  
* `vue --version` `
  * z.B. 3.9.1

## Installation
```
npm i
```

## Start
* json-server & vue (empfohlen)
  * ```npm run dev```
  
* nur vue
  * ```npm run serve```
  
* nur json-server
  * ```npm run server```
  
* unit tests
  * ```npm run test:unit```

